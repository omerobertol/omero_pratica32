/**
 *
 * @author Omero Francisco Bertol (27/09/2016)
 */

public class Pratica32 {

  public static void main(String[] args) {
      
    // Dentro do método main faça uma chamada para a função densidade, passando valores para x, media e desvio de -1, 67 e 3 respectivamente. 
    // Exiba o valor calculado.
    System.out.println("Densidade = " + densidade(-1, 67, 3));
  }
  
  public static double densidade(double x, double media, double desvio) {
    double d;
    
    d = (1 / (Math.sqrt(2 * Math.PI) * desvio)) * Math.exp(-(1 / 2.0) * Math.pow((x - media) / desvio, 2));
    
    return d;
  }
  
}